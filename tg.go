package main

import (
	"log"

	"gopkg.in/telegram-bot-api.v4"
)

var (
	botApi = "123123123123"
)

func initBot() {
	bot, err := tgbotapi.NewBotAPI(botApi)
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Can't understand. Please, check description with list of commands.")
		msg.ReplyToMessageID = update.Message.MessageID
		/*
			switch update.Message.Text {
			case "a":
				msg = tgbotapi.NewMessage(update.Message.Chat.ID, "aaa")
			case "/users":
				msg = tgbotapi.NewMessage(update.Message.Chat.ID, utils.StatUsers())
			case "/products":
				msg = tgbotapi.NewMessage(update.Message.Chat.ID, utils.StatProducts())
			}
		*/
		bot.Send(msg)
	}
}
