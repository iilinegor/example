package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
	"unicode/utf8"

	"./utils"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var (
	isTLS = "false"
	host  = "host"
)

func init() {
	log.Println("GODB: initiation...")

	arg := os.Args[1:]
	if len(arg) > 0 {
		isTLS = string(arg[0])
	}

	go utils.TestLog()
	Router()
}

func main() {
	log.Println("GoDB: started.")
}

// Router contains handlers for requests
func Router() {
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{}))
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Level: 5,
	}))

	if isTLS == "true" {
		e.Pre(middleware.HTTPSRedirect())
	}

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Golang version of VODB")
	})

	r := e.Group("/restricted")
	e.POST("/user/discover", userDiscover)
	e.POST("/login", login)

	r.Use(middleware.JWT([]byte("secret")))

	r.GET("/products/:id", utils.Prd.Get)
	r.POST("/products/new", utils.Prd.Create)
	r.POST("/products/delete", utils.Prd.Delete)
	r.POST("/products/update/peoples", utils.Prd.UpdateProduct)
	e.POST("/products/views/inc", utils.Prd.View)

	e.GET("/projects/:id", utils.Prj.Get)
	r.POST("/projects/update", utils.Prj.UpdateProject)
	r.POST("/projects/patch", utils.Prj.Patch)

	e.GET("/event", utils.Ev.Get)
	r.POST("/event/new", utils.Ev.Create)
	r.POST("/event/delete", utils.Ev.Delete)
	r.POST("/event/update", utils.Ev.UpdateEvent)

	e.GET("/comp", utils.Co.Get)
	r.POST("/comp/new", utils.Co.Create)
	r.POST("/comp/delete", utils.Co.Delete)
	r.POST("/comp/update", utils.Co.UpdateComp)

	e.GET("/post", utils.Po.Get)
	r.POST("/post/new", utils.Po.Create)
	r.POST("/post/delete", utils.Po.Delete)
	r.POST("/post/update", utils.Po.UpdatePost)

	e.GET("/user/:id", utils.Us.Get)
	r.POST("/user/new", utils.Us.Create)
	r.POST("/user/update", utils.Us.UpdateUser)

	r.GET("", restricted)

	if isTLS == "false" {
		e.Logger.Fatal(e.Start(":3035"))
	} else {
		e.Logger.Fatal(e.StartTLS(":3035", "/etc/letsencrypt/live/"+host+"/cert.pem", "/etc/letsencrypt/live/"+host+"/privkey.pem"))
	}
}

func restricted(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	name := claims["name"].(string)
	return c.String(http.StatusOK, "Welcome "+name+"!")
}

func login(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")

	users := *utils.Us.GetUsers().(*[]utils.User)

	for _, user := range users {

		if username == user.Email && password == user.Password {
			token := jwt.New(jwt.SigningMethodHS256)

			claims := token.Claims.(jwt.MapClaims)
			claims["name"] = user.Email
			claims["admin"] = true
			claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

			t, err := token.SignedString([]byte("secret"))
			if err != nil {
				return err
			}
			return c.JSON(http.StatusOK, map[string]string{
				"token": t,
			})
		}

	}

	log.Println(users[0].Email)

	return echo.ErrUnauthorized
}

func userDiscover(c echo.Context) error {
	username := c.FormValue("username")
	users := *utils.Us.GetUsers().(*[]utils.User)

	log.Println(username)

	for _, user := range users {
		if username == user.Email {
			return c.JSON(http.StatusOK, map[string]string{
				"email":   user.Email,
				"passlen": strconv.Itoa(utf8.RuneCountInString(user.Password)),
			})
		}

	}

	return echo.ErrUnauthorized
}
