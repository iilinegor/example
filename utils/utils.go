package utils

import (
	"log"
	"reflect"

	"gopkg.in/mgo.v2"
)

// Ev for event
var (
	session *mgo.Session
	Ev      Collection
	Co      Collection
	Po      Collection
	Us      Collection
	Prj     Collection
	Prd     Collection

	dbHost = "host"
	dbPass = "pass"
)

// Collection contains metadata for db collections
type Collection struct {
	col    *mgo.Collection
	datas  interface{}
	data   interface{}
	change string
}

// Collectioner is common collections interface
type Collectioner interface {
	Get()
	Create()
	Update()
	Delete()
	View()
	Patch()
}

func init() {
	log.Println("db connection...")
	session, err := mgo.Dial("mongodb://service:" + dbPass + "@" + dbHost + ":27017/test")
	if err != nil {
		log.Panic(err)
	}
	log.Println("db initiation...")

	Ev = Collection{session.DB("test").C("events"), new([]Event), new(Event), "events"}
	Prd = Collection{session.DB("test").C("market"), new([]Product), new(Product), "products"}
	Prj = Collection{session.DB("test").C("project"), new([]Project), new(Project), "projects"}
	Us = Collection{session.DB("test").C("users"), new([]User), new(User), "users"}
	Co = Collection{session.DB("test").C("comps"), new([]Comp), new(Comp), "comps"}
	Po = Collection{session.DB("test").C("posts"), new([]Post), new(Post), "posts"}

	log.Println(reflect.TypeOf(Ev.datas))
}
