package utils

import (
	"gopkg.in/mgo.v2/bson"
)

// Meta tmp type, not used
type Meta struct {
	ID bson.ObjectId `json:"_id" bson:"_id,omitempty"`
}

// Event is db collection schema
type Event struct {
	ID         bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Title      string        `json:"title"`
	Level      string        `json:"level"`
	Contacts   string        `json:"contacts"`
	Link       string        `json:"link"`
	Location   string        `json:"location"`
	Time       string        `json:"time"`
	FocusGroup string        `json:"focusGroup"`
	Experts    string        `json:"experts"`
	Members    string        `json:"members"`
	Progrem    string        `json:"program"`
	Org        string        `json:"org"`
	DayID      int64         `json:"id" bson:"id"`
	Len        int64         `json:"len"`
	Comp       int64         `json:"comp"`
	Closed     bool          `json:"closed"`
}

// Comp is db collection schema
type Comp struct {
	ID     bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Title  string        `json:"title"`
	Color  string        `json:"color"`
	Closed bool          `json:"closed"`
}

// Post is db collection schema
type Post struct {
	ID       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Title    string        `json:"title"`
	Author   string        `json:"author"`
	AuthorID string        `json:"authorId"`
	Text     string        `json:"text"`
	Date     int64         `json:"date"`
	Views    int64         `json:"views"`
	Private  bool          `json:"private"`
}

// Product is db collection schema
type Product struct {
	ID          bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Name        string        `json:"name"`
	Description string        `json:"description"`
	AuthorID    string        `json:"authorId" bson:"authorId"`
	Unique      string        `json:"unique"`
	Peoples     []string      `json:"peoples"`
	Type        int           `json:"type"`
	Subtype     int           `json:"subtype"`
	Year        int           `json:"year"`
	Area        int           `json:"area"`
	Views       int           `json:"views"`
	LastViews   int           `json:"lastViews"`
	Rank        int           `json:"rank"`
	Date        int64         `json:"date"`
	Industry    []int         `json:"industry"`
	Closed      bool          `json:"closed"`
	Private     bool          `json:"private"`
}

// Social is User's subtype
type Social struct {
	Facebook  string `json:"facebook"`
	Vk        string `json:"vk"`
	Instagram string `json:"instagram"`
	Github    string `json:"github"`
	Habrahabr string `json:"habrahabr"`
	Dnevnik   string `json:"dnevnik"`
}

// User is db collection schema
type User struct {
	ID          bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	Name        string        `json:"name"`
	Photo       string        `json:"photo"`
	Email       string        `json:"email"`
	Password    string        `json:"password"`
	Phone       string        `json:"phone"`
	Location    string        `json:"location"`
	Projects    []string      `json:"projects"`
	Worktree    string        `json:"worktree"`
	RegisterAt  int64         `json:"registerAt"`
	Description string        `json:"description"`
	Date        int64         `json:"date"`
	Type        []int         `json:"type"`
	Todos       []Todo        `json:"todos"`
	Social      Social        `json:"social"`
}

// Todo is User's subtype
type Todo struct {
	Title  string `json:"title"`
	Active bool   `json:"active"`
	Date   int64  `json:"date"`
}

// Patch is db collection schema
type Patch struct {
	Op       string                 `json:"op"`
	Path     string                 `json:"path"`
	AuthorID string                 `json:"authorId"`
	Value    map[string]interface{} `json:"value"`
	Date     int64                  `json:"date"`
}

// PatchRaw is Patch's ext version
type PatchRaw struct {
	Op        string                 `json:"op"`
	Path      string                 `json:"path"`
	AuthorID  string                 `json:"authorId" bson:"authorId"`
	Value     map[string]interface{} `json:"value"`
	Date      int64                  `json:"date"`
	ProjectID string                 `json:"projectId" bson:"projectId"`
	Type      string                 `json:"type"`
}

// Processes is Project's subtype
type Processes struct {
	Name  string `json:"name"`
	Title string `json:"title"`
}

// Project is db collection schema
type Project struct {
	ID        bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	ProjectID string        `json:"projectId" bson:"projectId"`
	Processes []Processes   `json:"processes"`
	Roadmap   []Patch       `json:"roadmap"`
	Money     []Patch       `json:"money"`
	Date      int64         `json:"date"`
}
