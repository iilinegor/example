package utils

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"golang.org/x/net/websocket"
)

var (
	cons   int
	Change string = "nope"
	isTLS  string = "false"
)

type Stat struct {
	Online int    `json:"online"`
	Size   int    `json:"size"`
	Target string `json:"target"`
}

func TestLog() {
	arg := os.Args[1:]
	if len(arg) > 0 {
		isTLS = string(arg[0])
	}

	log.Println("Websocket started")
	http.Handle("/", websocket.Handler(echoHandler))

	if isTLS == "false" {
		err := http.ListenAndServe("0.0.0.0:1010", nil)
		if err != nil {
			panic("ListenAndServe: " + err.Error())
		}
	} else {
		err := http.ListenAndServeTLS("0.0.0.0:1010", "/etc/letsencrypt/live/skyleap.ru/cert.pem", "/etc/letsencrypt/live/skyleap.ru/privkey.pem", nil)
		if err != nil {
			panic("ListenAndServe: " + err.Error())
		}
	}
}

func echoHandler(ws *websocket.Conn) {
	defer func() {
		cons--
		ws.Close()
	}()
	cons++
	var s Stat
	var lastCons int

	log.Println(ws.RemoteAddr().String() + " - " + ws.Request().RemoteAddr + " - " + ws.Request().UserAgent())

	msg := make([]byte, 512)
	n, err := ws.Read(msg)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Receive: %s\n", msg[:n])
	for {
		if Change == "nope" && cons != lastCons {
			s.Online = cons
			s.Size = 51241
			//			log.Println(cons)
			lastCons = cons
		} else {
			s.Target = Change
			Change = "nope"
		}
		stat, _ := json.Marshal(s)
		//		log.Println("send: ", string(stat))
		_, err := ws.Write(stat)
		if err != nil {
			break
		}

		time.Sleep(1 * time.Second)

	}
}
