package utils

import (
	"log"
	"net/http"
	"reflect"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2/bson"
)

// Get object from col
func (c *Collection) Get(ec echo.Context) error {
	id := ec.Param("id")

	switch {
	case len(id) == 24:
		c.data = (reflect.Zero(reflect.ValueOf(c.data).Type()))

		err := c.col.Find(bson.M{"_id": bson.ObjectIdHex(string(id))}).One(&c.data)
		if c.change == "projects" {
			err = c.col.Find(bson.M{"projectId": (string(id))}).One(&c.data)
		}

		if err != nil {
			return ec.String(http.StatusOK, "not found")
		}

		return ec.JSON(http.StatusOK, c.data)
	case len(id) < 24:
		err := c.col.Find(bson.M{}).Sort("-date").All(c.datas)
		if err != nil {
			log.Fatal(err)
		}

		return ec.JSON(http.StatusOK, c.datas)
	default:
		err := c.col.Find(bson.M{}).Sort("-date").All(c.datas)
		if err != nil {
			log.Fatal(err)
		}

		return ec.JSON(http.StatusOK, c.datas)
	}
}

// GetUsers returns users list
func (c *Collection) GetUsers() interface{} {
	// users := make([]User, 0, 100)
	c.col.Find(bson.M{}).Sort("-date").All(c.datas)
	return c.datas
}

// Create new object in col
func (c *Collection) Create(ec echo.Context) error {
	i := bson.NewObjectId()

	if err := ec.Bind(c.data); err != nil {
		return err
	}

	if c.change == "products" {
		box := c.data.(*Product)
		box.ID = i
		c.col.Insert(box)
		p := bson.M{"projectId": i.String()[13:37], "processes": []Processes{Processes{Name: "dev", Title: "New Process"}}, "roadmap": []Patch{}, "money": []Patch{}, "date": 1241231}
		Prj.col.Insert(p)
	} else {
		c.col.Insert(c.data)
	}

	Change = c.change
	return ec.JSON(http.StatusCreated, c.data)
}

// UpdateUser updates user
func (c *Collection) UpdateUser(ec echo.Context) error {
	u := new(User)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// UpdateProduct updates product
func (c *Collection) UpdateProduct(ec echo.Context) error {
	u := new(Product)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// UpdateProject updates project
func (c *Collection) UpdateProject(ec echo.Context) error {
	u := new(Project)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// UpdateEvent updates event
func (c *Collection) UpdateEvent(ec echo.Context) error {
	u := new(Event)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// UpdateComp updates comp
func (c *Collection) UpdateComp(ec echo.Context) error {
	u := new(Comp)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// UpdatePost updates post
func (c *Collection) UpdatePost(ec echo.Context) error {
	u := new(Post)
	if err := ec.Bind(u); err != nil {
		return err
	}
	c.col.UpdateId(u.ID, u)

	return ec.JSON(http.StatusCreated, u)
}

// Update object in col
func (c *Collection) Update(ec echo.Context) error {
	if err := ec.Bind(&c.data); err != nil {
		return err
	}

	log.Printf("%T", (c.data).(map[string]interface{})["_id"])
	str := (c.data).(map[string]interface{})["_id"].(string)

	log.Printf("%T", (c.data).(map[string]interface{})["_id"])
	log.Println(bson.ObjectIdHex(str))

	c.col.Update(bson.M{"_id": str}, c.data)

	Change = c.change
	return ec.JSON(http.StatusCreated, c.data)
}

// Patch: build and save
func (c *Collection) Patch(ec echo.Context) error {
	var p PatchRaw
	if err := ec.Bind(&p); err != nil {
		return err
	}

	var patch Patch

	patch.Op = p.Op
	patch.Path = p.Path
	patch.Value = p.Value
	patch.Date = p.Date
	patch.AuthorID = p.AuthorID

	switch p.Type {
	case "roadmap":
		c.col.Update(bson.M{"projectId": p.ProjectID}, bson.M{"$push": bson.M{"roadmap": patch}})
	case "money":
		c.col.Update(bson.M{"projectId": p.ProjectID}, bson.M{"$push": bson.M{"money": patch}})

	}

	Change = c.change
	return ec.JSON(http.StatusCreated, patch)
}

// View increments number of views
func (c *Collection) View(ec echo.Context) error {
	if err := ec.Bind(c.data); err != nil {
		return err
	}
	c.col.Update(c.data, bson.M{"$inc": bson.M{"views": 1}})
	Change = c.change
	return ec.JSON(http.StatusCreated, c.data)
}

// Delete object from col
func (c *Collection) Delete(ec echo.Context) error {
	if err := ec.Bind(c.data); err != nil {
		return err
	}
	c.col.Remove(c.data)
	if c.change == "products" {
		id := c.data.(*Product).ID
		log.Println("del prj: ", id.String()[13:37])
		Prj.col.Remove(bson.M{"projectId": id.String()[13:37]})
	}
	Change = c.change
	return ec.String(http.StatusCreated, "deleted")
}
